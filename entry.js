import 'expo/build/Expo.fx';
import React from 'react';
import { AppRegistry } from 'react-native';
import withExpoRoot from 'expo/build/launch/withExpoRoot';

import { createRoot } from 'react-dom/client';
import App from './App';
import { isWeb } from './utilities';

AppRegistry.registerComponent('main', () => withExpoRoot(App));
if (isWeb) {
  const rootTag = createRoot(document.getElementById('root') ?? document.getElementById('main'));
  const RootComponent = withExpoRoot(App);
  rootTag.render(<RootComponent />);
}
