import identityReducer from './identitySlice';

export default {
  identity: identityReducer,
};
