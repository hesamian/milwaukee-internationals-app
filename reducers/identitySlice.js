import { createSlice } from '@reduxjs/toolkit';
import {
  setPushNotificationToken,
  setUserId,
} from '../services/identityService';
import { asyncReducer } from '../utilities';

const initialState = {
  pushNotificationToken: null,
  userId: null,
  loading: false,
  error: null,
};

export const identitySlice = createSlice({
  name: 'identity',
  initialState,
  reducers: {},
  extraReducers: {
    ...asyncReducer(setPushNotificationToken, (state, payload) => {
      state.pushNotificationToken = payload;
    }),
    ...asyncReducer(setUserId, (state) => {
      state.pushNotificationToken = null;
    }),
  },
});

// Action creators are generated for each case reducer function
export default identitySlice.reducer;
