import { prefixUrl } from '../utilities/url';

export const sendHttpLog = (log) => fetch(prefixUrl('/api/log'), {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(log),
});
