import { post } from '../utilities';

export const validateUserId = async (userId) => {
  const { result } = await post('/api/driver/login/validate', {
    userId,
  });
  return result;
};

export const authenticateUserId = async (userId) => {
  const { result } = await post('/api/driver/login', {
    userId,
  });
  return result;
};

export const assignPushNotificationToken = async (userId, token) => {
  const { result } = await post('/api/PushNotification/token', {
    userId,
    token,
  });
  return result;
};
