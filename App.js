import 'text-encoding-polyfill';
import 'react-native-gesture-handler';
import React from 'react';
import Index from './index';
import { AppCrashWrapper } from './utilities';

export default function () {
  return (
    <AppCrashWrapper>
      <Index />
    </AppCrashWrapper>
  );
}
