import { createAction } from '../utilities';
import { PUSH_NOTIFICATION, USER_ID } from '../constants';
import { setStorageData } from '../services/storageService';

export const setUserId = createAction(
  USER_ID,
  async (id, key) => {
    await setStorageData(key, id);
    return id;
  },
);

export const clearUserId = createAction(
  USER_ID,
  async (id, key) => {
    await setStorageData(key, null);
    return id;
  },
);

export const setPushNotificationId = createAction(
  PUSH_NOTIFICATION,
);
