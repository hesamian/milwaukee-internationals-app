import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import { RenderHtml } from '../../utilities';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flex: 1,
  },
});

export default function About() {
  return (
    <View style={styles.container}>
      <RenderHtml>
        <div style={{ marginLeft: '1rem', marginRight: '1rem' }}>
          <h2>Milwaukee-Internationals</h2>
          <p>
            Welcome to milwaukee-internationals about page.
            Please contact Asher Imtiaz for more information.
          </p>
          <h4>Credits:</h4>
          <ul>
            <li>
              Asher Imtiaz
            </li>
            <li>
              Amir Hesamian
            </li>
          </ul>
        </div>
      </RenderHtml>
    </View>
  );
}
