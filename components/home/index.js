import React, { useEffect, useRef, useState } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import * as Notifications from 'expo-notifications';
import { useDispatch } from 'react-redux';
import Login from '../login';
import { validateUserId } from '../../services/identityService';
import { getStorageData, setStorageData } from '../../services/storageService';
import { USER_ID } from '../../constants';
import { clearUserId } from '../../actions/identityAction';
// import { useSelector } from 'react-redux';

const Drawer = createDrawerNavigator();

export default function Home() {
  // const token = useSelector((state) => state.identity.pushNotificationToken);
  // const error = useSelector((state) => state.identity.error);
  // const [driverId, setDriverId] = useState('');
  const dispatch = useDispatch();

  const [, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
    getStorageData(USER_ID).then(async (savedUserId) => {
      if (savedUserId) {
        if (await validateUserId(savedUserId)) {
          // All good
        } else {
          await dispatch(clearUserId()).unwrap();
        }
      }
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  return (
    <Drawer.Navigator initialRouteName="Login">
      <Drawer.Screen name="Login" component={Login} />
    </Drawer.Navigator>
  );
}
