import React, { useState } from 'react';
import {
  StyleSheet, Button, View, Text, SafeAreaView, TextInput,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { authenticateUserId, validateUserId } from '../../services/identityService';
import { setUserId } from '../../actions/identityAction';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    width: 250,
    height: 44,
    padding: 10,
    marginTop: 20,
    marginBottom: 10,
    backgroundColor: '#e8e8e8',
  },
  error: {
    color: 'red',
  },
});

export default function Login({ navigation }) {
  const dispatch = useDispatch();
  const [driverId, setDriverId] = useState('');
  const [invalidDriverId, setInvalidDriverId] = useState(false);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Text>Welcome To Milwaukee Internationals!</Text>
        { invalidDriverId
          ? <Text style={styles.error}>DriverId is not valid, please try again!</Text> : null}
        <TextInput
          style={styles.input}
          value={driverId}
          placeholder="Driver ID"
          onChangeText={(value) => setDriverId(value)}
        />
        <Button
          title="Enter"
          onPress={async () => {
            if (await validateUserId(driverId)) {
              setInvalidDriverId(false);
              await authenticateUserId(driverId);
              await dispatch(setUserId(driverId)).unwrap();
              navigation.navigate('About');
            } else {
              setInvalidDriverId(true);
            }
          }}
        />
      </View>
    </SafeAreaView>
  );
}
