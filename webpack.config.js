const createExpoWebpackConfigAsync = require('@expo/webpack-config');
const Agent = require('agentkeepalive');
const constants = require('./constants');

function proxy(urls) {
  return urls.reduce((acc, v) => Object.assign(acc, acc, {
    [v]: {
      target: constants.API_BASE_URL,
      changeOrigin: true,
      logLevel: 'debug',
      ws: true,
      agent: new Agent({
        maxSockets: 100,
        keepAlive: true,
        maxFreeSockets: 10,
        keepAliveMsecs: 1000,
        timeout: 60000,
        freeSocketTimeout: 30000,
      }),
    },
  }), {});
}

// Expo CLI will await this method, so you can optionally return a promise.
// eslint-disable-next-line func-names
module.exports = async function (env, argv) {
  const config = await createExpoWebpackConfigAsync(env, argv);

  // Maybe you want to turn off compression in dev mode.
  if (config.mode === 'development') {
    config.devServer.proxy = proxy(['/api', '/log']);
    config.devServer.clientLogLevel = 'info';
    config.devServer.compress = false;
  }

  // Or prevent minimizing the bundle when you build.
  if (config.mode === 'production') {
    config.optimization.minimize = false;
  }

  // Finally return the new config for the CLI to use.
  return config;
};
