import { LOG } from './logger';
import { prefixUrl } from './url';

async function buildFetchRequest(type, path, body = null) {
  const requestOptions = {
    method: type,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };

  // Add body if needed
  if (body) {
    requestOptions.body = JSON.stringify(body);
  }
  const url = prefixUrl(path);

  try {
    LOG.info(`Attempting REST call [${type}]: ${url}`);
    const response = await fetch(url, requestOptions);
    const re = response.json();
    LOG.info(`Successful REST call [${type}]: ${url}`);
    return re;
  } catch (err) {
    LOG.error(err);
    throw new Error(`${type} ${url} failed with response.`);
  }
}

export function get(path) {
  return buildFetchRequest('GET', path);
}

export function post(path, body) {
  return buildFetchRequest('POST', path, body);
}

export function put(path, body) {
  return buildFetchRequest('PUT', path, body);
}

// prefixed with underscored because delete is a reserved word in javascript
// eslint-disable-next-line no-underscore-dangle
export function _delete(path) {
  return buildFetchRequest('DELETE', path);
}
