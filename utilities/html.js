import React from 'react';
import { useWindowDimensions } from 'react-native';
import ReactRenderHtml from 'react-native-render-html';
import ReactDOMServer from 'react-dom/server';

export function RenderHtml({ children }) {
  const { width } = useWindowDimensions();
  const html = ReactDOMServer.renderToStaticMarkup(children);
  return (
    <ReactRenderHtml
      contentWidth={width}
      source={{
        html,
      }}
    />
  );
}
