import { Platform } from 'react-native';

export const isDevelopment = process.env.NODE_ENV === 'development';
export const isWeb = Platform.OS === 'web';
