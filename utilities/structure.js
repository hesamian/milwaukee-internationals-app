import React from 'react';
import { Text } from 'react-native';
import { LOG } from './logger';

export const AppCrashWrapper = ({ children: Component }) => {
  try {
    return (<Component />);
    // eslint-disable-next-line no-unreachable
  } catch (err) {
    LOG.error('App crashes', err);
    return (
      <Text>
        Application Crashed:
        {err.message}
      </Text>
    );
  }
};
