import { isDevelopment } from './env';
import { API_BASE_URL } from '../constants';

export function prefixUrl(url) {
  if (isDevelopment) {
    return url;
  }
  return API_BASE_URL + url;
}
