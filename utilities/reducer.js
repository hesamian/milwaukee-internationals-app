export const asyncReducer = (action, handler) => ({
  [action.pending]: (state) => {
    state.loading = true;
  },
  [action.fulfilled]: (state, { payload }) => {
    state.loading = false;
    handler(state, payload);
  },
  [action.rejected]: (state, error) => {
    state.loading = false;
    state.error = error;
  },
});
