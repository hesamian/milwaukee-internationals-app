import 'react-native-get-random-values';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';

export const createAction = (url, handler = null) => createAsyncThunk(
  `${url}#${uuidv4()}`,
  (arg) => (handler ? handler(arg, url) : arg),
);
