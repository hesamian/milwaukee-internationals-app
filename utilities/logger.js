import { logger, consoleTransport } from 'react-native-logs';
import { sendHttpLog } from '../services/logService';
import { signalRInvoke } from './signalr';

const customTransport = async (props) => {
  // Do here whatever you want with the log message
  // You can use any options set in config.transportOptions
  // Eg a console log: console.log(props.level.text, props.msg)

  consoleTransport(props);

  if (props.level.text === 'error') {
    // If SignalR failed when try http logging
    if (!await signalRInvoke('Sink', props)) {
      await sendHttpLog(props);
    }
  }
};

export const LOG = logger.createLogger({
  transport: customTransport,
});
