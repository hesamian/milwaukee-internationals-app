import * as signalRBuilder from '@microsoft/signalr';
import { LogLevel, HubConnectionState, JsonHubProtocol } from '@microsoft/signalr';
import { prefixUrl } from './url';

const signalR = new signalRBuilder.HubConnectionBuilder()
  .withUrl(prefixUrl('/log'))
  .configureLogging(LogLevel.Trace)
  .withAutomaticReconnect()
  .withHubProtocol(new JsonHubProtocol())
  .build();

signalR.start();

export const signalRInvoke = async (method, arg) => {
  try {
    if (signalR?.state === HubConnectionState.Connected) {
      await signalR.invoke(method, arg);
      return true;
    }
    return false;
  } catch (err) {
    return false;
  }
};
