module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'no-unused-expressions': 'off',
    'no-param-reassign': 'off',
    'react/jsx-filename-extension': 'off',
    'no-alert': 'off',
    'react/prop-types': 'off',
    'no-restricted-syntax': 'off',
    'no-continue': 'off',
    'import/prefer-default-export': 'off',
  },
};
